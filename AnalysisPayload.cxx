// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1F.h>
#include <TLorentzVector.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"


int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // Making Histograms
  TH1F* jet_number = new TH1F("njets","njets",30,0,30);
  TH1F* dijet_mass = new TH1F("dijet_mass","dijet_mass",250,0,500);



  ///////////////////////
  // Code begins below //
  ///////////////////////
  
  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {
  // Line below is for debugging purposes, it probes a small number of events
  // for ( Long64_t i=0; i<20; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    // std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;


    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // Printing how many jets are in the event, and filling a histo with it.
    // std::cout << "Number of jets: nJets=" << jets->size() << std::endl;
    jet_number->Fill(jets->size());


    // Defining a vector that will store all the jets that pass a certian selction criteria 
    std::vector< xAOD::Jet > event_jets;


    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      
      // print the kinematics of each jet in the event
      // std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;


      // Selecting jets that are over 50GeV
      if(jet->pt() > 50E3 && std::abs(jet->eta()) < 2.5){
        // Filling the array with all the jets of any event 
      event_jets.push_back(*jet);

      }
      
      
    }


    if(event_jets.size() >=2){
      dijet_mass->Fill( (event_jets.at(0).p4() + event_jets.at(1).p4()).M()/1000. );
      
    }
    

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // creating a new root file to store histograms in
  // TFile f("histos.root","new");

  // The way thats done in the solutions of the bootcamp
  TFile *fout = new TFile("histos.root","RECREATE");

  //Drawing the relevant histograms
  jet_number->Draw("HIST");
  dijet_mass->Draw("HIST");
  jet_number->Write();
  dijet_mass->Write();

  // Closing out the histogram root file
  fout->Close();


  // exit from the main function cleanly
  return 0;
}